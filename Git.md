# Git

## Initialisation d'un projet base

<span style="color:yellow">@tags: </span><span style="color:red"> start init config </span>

* git config --global user.email sam@google.com

* git

* git init

* git add .

* git commit  -m  " [dev master etc ] nom du commit"

### Ajout du dépot distant

<span style="color:yellow">@tags: </span><span style="color:red"> dépot distant remote</span>

Cette commande permet de se connecter à un dépot distant.

* git remote add origin  http://depodistant------/--/



### **Git tag**

- Le marquage est utilisé pour marquer des commits spécifiques avec des poignées simples. Un exemple peut être:
  
  git tag 1.1.0 <insert-commitID-here>
  
  

### **Git reset**

- Pour réinitialiser l’index et le répertoire de travail à l’état du dernier commit, la **commande git reset** est utilisée :
  
  git reset --hard HEAD



### **Git rm**

- **Git rm** peut être utilisé pour supprimer des fichiers de l’index et du répertoire de travail. Usage:
  
  git rm nomfichier.txt
  
  

### **Git fetch**

- **Git fetch** permet à un utilisateur d’extraire tous les fichiers du dépôt distant qui ne sont pas actuellement dans le répertoire de travail local. Exemple d’utilisation:
  
  git fetch origin
  
  

### **Git rebase**

- La **commande git rebase** est utilisée pour la réapplication des commits sur une autre branche. Par exemple:
  
  git rebase master
