# MarkDown

# Titres

 #=> faire un h1

# h1

##=> faire un h2

## h2

###=>faire un h3

### h3

####=> faire un h4

#### h4

# liste non ordonnée

*=> élément

*=> élément 2

*=> élément 3

* élément 1

* élément 2

* élément 3

# liste ordonnée

1.=> élément 1

2.=> élément 2

1. élément 1

2. élément 2

# Texte

### texte en italique

étoile début -> *texxxxt ttexx tteeex texx*  <- étoile fin

### Texte en gras

2 étoile début ->****texxxxt ttexx tteeex texx****<- 2 étoile fin

```
[left]Texte àligné à gauche[/left]
[center]Texte centré[/center]
[right]Texte algné à droite[/right]
```

### Texte souligné

2  tréma début ->____texxxxt ttexx tteeex texx____<- 2 tréma fin

# Texte color

texte color red <span style="color:red"> kekekeke </span>

```html
texte color red <span style="color:red">blabalbal</span>
texte color blue <span style="color:blue">blabalbal</span>
texte color green <span style="color:green">blabalbal</span>
texte color teeal <span style="color:teal">blabalbal</span>
```

# Lien

Sans espace, à retirer ( / )

[nom du site]/(adresse http du site)

[amazon](http://amazon.com)

[la poste](http://amazon.com)

# image

![alt img](C:\Users\kat\Documents\Documentations\linux.png)

Sans espace, à retirer ( / )

!/[image name = alt ]/( img link http)

# Citation

[>]citation ou phrase

> citation 1
> 
> citation 2

# Code

```html
 <div id="about"></div>

          It works!
 </div>
```

CTRL+ALT+C fait apparaitre  un bloc de code.

# Table

CRTL+MAG+T

| SSS | SSSSSSSSS | SSSSSSSSSS     |     |     |
| ---:| --------- | -------------- | --- | --- |
| SSS | SSSSSSS   | SSSSSSSSS      |     |     |
| SS  | S         | SSSSSSSSSSSSSS |     |     |
