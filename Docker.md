# Docker Documentation

---



### Lancer un containeur à partir d'une image en arrière plan

docker run -it -d --name mon_container_alpine2 alpine

```shell
docker run -it -d  --name dala_name img_alpine

```

### Démarrer un containeur

<span style="color:yellow">@tags: </span><span style="color:red"> start  </span>

docker start conaitner_name

```shell
docker start jenkins 
```

### Rentrer dans un containeur

<span style="color:yellow">@tags: </span><span style="color:red"> exec - bash </span>

docker exec -it conaitner_name bash

```shell
docker exec -it jenkins bash
```



lancer un containeur et le laisser tourné en tâche de fond

```markup
docker run -it -d jenkins
```



supprimer un containeur

docker rm -f containeur_name









Voir les containeurs en mode run

docker ps



Voir tous les containeurs

docker ps -a
